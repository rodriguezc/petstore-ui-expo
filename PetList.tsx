import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';

type statusType = "available" | "pending" | "sold"
interface Pet {
    id: number,
    name: string,
    category: {
        id: number,
        name: string
    },
    status: statusType
}

const PetList: React.FC = () => {

    const [status, setStatus] = useState<statusType>("available");
    const [pets, setPets] = useState<Pet[]>([]);

    useEffect(() => {
        fetch(`https://5be1140d.ngrok.io/api/v3/pet/findByStatus?status=${status}`).then((response) => {
            response.json().then(function (resPets) {
                setPets(resPets.slice(0, 10));
            });
        });

    }, [status]);

    return (
        <View>
            <Text>Salut mec</Text>
            {pets.map((pet, index) => <Text key={index}>{pet.id} - {pet.name}</Text>)}
        </View>
    );
}

export default PetList;
